\documentclass[a4paper]{article}

\usepackage{fontspec}
\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}

\usepackage{microtype}

\usepackage{graphicx}
\usepackage{wrapfig}

\usepackage{hyperref}
\hypersetup{
  pdfauthor = {Nikolaos Pothitos},
  pdftitle = {Annual Report for the PhD Thesis "Constraint
              Programming: Algorithms and Systems"}
}

\begin{document}

\begin{wrapfigure}[2]{L}{3.4em}
  \includegraphics[width=3em]{athena}
\end{wrapfigure}
\noindent
\textbf{Εθνικό και Καποδιστριακό Πανεπιστήμιο Αθηνών
        \\[0.5em]
        Τμήμα Πληροφορικής και Τηλεπικοινωνιών}

\vspace{3em}

\noindent
Αριθμός Πρωτοκόλλου: \\
Πρωτοκολλήθηκε την:

\vspace{1.3em}

\begin{center}
  \textbf{Υπόμνημα Ετήσιας Προόδου Διδακτορικής Διατριβής
          \\[0.5em]
          Ακαδημαϊκό Έτος 2019–2020}
\end{center}

\vspace{1em}

\noindent
Επώνυμο: Ποθητός \\
Όνομα: Νικόλαος \\
Αριθμός Μητρώου: Δ541 \\
Τίτλος Διατριβής: Προγραμματισμός με Περιορισμούς:
                  Αλγόριθμοι και Συστήματα \\
Επιβλέπων Καθηγητής: Παναγιώτης Σταματόπουλος \\
Σύμβουλος Καθηγητής: Κωνσταντίνος Χαλάτσης \\
Σύμβουλος Καθηγητής: Σταύρος Κολλιόπουλος

\vspace{1em}

Κατά το ακαδημαϊκό έτος 2019–2020 ολοκληρώθηκε μία ακόμα
δημοσίευση των αποτελεσμάτων της διατριβής σε έγκριτο
περιοδικό. Το άρθρο με τίτλο \emph{The Dilemma Between Arc
and Bounds Consistency} δημοσιεύτηκε στον
35\textsuperscript{ο} τόμο του International Journal of
Intelligent Systems~\cite{Pothitos2020}.

Με αυτή τη δημοσίευση ολοκληρώνεται ένα σημαντικό μέρος της
διατριβής που αφορά στη διάδοση περιορισμών (constraint
propagation) η οποία είναι αναπόσπαστο κομμάτι του
Προγραμματισμού με Περιορισμούς. Οι λεπτομέρειες της
συνεισφοράς συνοψίζονται παρακάτω στην Αγγλική.

From Constraint Programming early years, developers of
solvers such as \textsf{Ilog} have observed empirically that
there is a trade-off between arc and bounds consistency in
terms of time and space, and bounds consistency is
preferable in many cases.

In alternative approaches to this work, in current
constraint programming solvers, the choice between AC and BC
is not justified theoretically but only empirically. In our
work, apart from wide experimental results, we provide
theoretical analysis for the AC vs.\ BC trade-off so as to
predict when arc consistency becomes a bottleneck. We show
that bounds consistency is usually more efficient when
dealing with CSPs having large domains.

This could be thought of as a paradox, because AC and BC
have equal worst-case complexities, and AC is stronger than
BC, in the sense that it removes more inconsistent values
out of the domains of constrained variables. This is true,
but only when we study the constraint propagation algorithms
isolated, independently of the search methods. In this work,
we try to see the big picture: constraint propagation
integrated into backtracking search methods. We compute the
\emph{overall} time complexity and focus on how it is
affected by the choice between AC and BC.

The main contribution of this work is to give focus on the
weaker consistency levels (BC) in Constraint Programming and
to highlight their advantages over ``stronger'' consistency
levels (AC). If we take it for granted that arc and bounds
consistency have both equal asymptotic time complexities,
then two questions arise.

\begin{enumerate}
  \item Why BC is often used in practice in Constraint
        Programming solvers?
  \item When should we prefer BC over AC?
\end{enumerate}

In current bibliography, answers to the first question are
scarce and only based on unpublished empirical observations.
In any case, one can answer to the first question by
conducting experiments and finding examples where BC is more
efficient than AC. Indeed, in this work, we experimented
with a broad range of official CSPs and found many cases
where BC is more efficient in practice.

The second question is more difficult, as it is addressed
for every possible ad hoc CSP. Our approach to answer it
included the following steps.

\begin{itemize}
  \item Introduce the algorithms for arc and bounds
        consistency enforcement and prove that they take the
        same time in the worst case.
  \item Introduce a basic backtracking search algorithm and
        the search tree and search path notions.
  \item Integrate consistency enforcement algorithms into
        the backtracking search method.
  \item Compute the overall time complexity while descending
        a search tree path and find the differentiations
        between maintaining AC and BC.
  \item Project the complexity to traverse a search path to
        the overall search tree complexity.
\end{itemize}

Following this approach, we produced some tight upper limits
for AC and BC time complexities in the context of search
methods. We defined a criterion which, based on the
attributes of a CSP, predicts which of the two methodologies
is likely to solve it faster.

This new criterion gives us the freedom to select the
consistency level (AC or BC) just before solving a specific
CSP. We are not obliged to use default consistency levels
when we build a Constraint Programming solver anymore. We
are now able to tailor the AC vs.\ BC selection to the
particular parameters of each CSP and thus make the overall
search process more efficient.

\bibliographystyle{IEEEtranS}
\bibliography{references}

\vspace{1em}

\begin{center}
  Αθήνα, 30/9/2020

  \vspace{4em}

  Υπογραφή Υποψήφιου Διδάκτορα
\end{center}

\end{document}
